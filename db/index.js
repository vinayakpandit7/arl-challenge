const mongoose = require('mongoose');
const { Schema } = mongoose;  


const shortUrlSchema = new Schema({
    id: { type: String, unique: true },
    short_url: String,
    original_url: String
});

const ShortURLModel = mongoose.model('ShortURL', shortUrlSchema, 'short_url');

module.exports = ShortURLModel;