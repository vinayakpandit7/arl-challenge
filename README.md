Steps to start server locally.
1. Install node.
2. In apps base directory(which has package,json) do -
    i. npm i
    ii. npm start
3. app will start listening on localhost:3000

API DOC
================
1. POST '{base-url}/create-shorter-url'
    This is post api which accepts 'original_url' as single JSON body parameter in which an original url which need to be shorten will be passed.

    This return an object having shorten URL in key named 'short_url'.

2. GET '{base-url}/url-shortner/:id'
    -calling this api will map to the original url map to this particular shorten url
    -:id is a path params which is a randomly generated string id at the time of shorten url creation.