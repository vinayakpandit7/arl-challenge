var express = require('express');
var router = express.Router();

var ShortURLModel = require('../db/index');

function makeid(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

/* GET home page. */
router.get('/url-shortner/:id', function(req, res, next) {

  let id = '';
  console.log(req.params)
  if(req.params.id){
    id = req.params.id
  }
  ShortURLModel.findOne({id:id}, (err, data)=>{
    console.log(data);
    let orgUrl = data.original_url;
    res.redirect(orgUrl);
  })  
});

router.post('/create-shorter-url', function(req, res, next) {

  let obj = {}
  if(req.body.originalUrl){
    obj.original_url = req.body.originalUrl;
  }
  let id = makeid(10);
  obj.id = id;
  obj.short_url = 'http://localhost:3000/url-shortner/' + id;
  ShortURLModel.create(obj, (err, data)=>{
    //data.short_url = 'http://localhost:3000/id';
    console.log(data);
    res.send(data);
  })
});

module.exports = router;
